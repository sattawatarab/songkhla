const key = 'sWQIrLpgU397fZdNrXD709kpA8GNhJtt7rSrfJYyMgyxVqlRShJT9x1CofmcsNpS'; // Your API key

// Initialize the map
const map = new maplibregl.Map({
  container: 'map', // ID of the map container
  style: `https://va-cdn-02.vallarismaps.com/core/api/styles/1.0-beta/styles/6796f5d2b7afe43c5898ff63?api_key=${key}`, // Map style
  center: [100.5018, 13.7563], // Initial center [lng, lat] (Bangkok)
  zoom: 5 // Initial zoom level
});

// Add navigation controls to the map
map.addControl(new maplibregl.NavigationControl(), 'top-left');

// Add a vector tile source for air quality data
map.on('load', () => {
    map.addSource('airquality', {
      type: 'vector',
      url: `https://va-cdn-02.vallarismaps.com/core/api/tiles/1.0-beta/tiles/6796f463b7afe43c5898ff62/{z}/{x}/{y}?api_key=${key}` // Replace with your vector tile URL
    });

    map.addLayer({
      id: 'airquality-layer',
      type: 'circle',
      source: '6796f463b7afe43c5898ff62',
      'source-layer': '6796f463b7afe43c5898ff62', // Replace with the actual source layer name
      paint: {
        'circle-radius': 10,
        'circle-color': '#00e400',
        'circle-opacity': 0.0
      }
    });

  // Update info box on hover
  map.on('mousemove', 'airquality-layer', (e) => {
    const features = e.features[0].properties;
    const aqi = features.aqiaqi || 'N/A';
    const pm25 = features.pm25 || 'N/A';
    const name = features.nameth || 'Unknown Location';
    const areath = features.areath || 'N/A';
    const date_get = features.date || 'N/A';
    const time_get = features.time || 'N/A';

    document.getElementById('info').innerHTML = `
      <strong>Air Quality Index:</strong> ${aqi}<br>
      <strong>PM 2.5:</strong> ${pm25} µg/m³<br>
      <strong>สถานที่:</strong> ${name}<br>
      <strong>พื้นที่:</strong> ${areath}<br>
      <strong>วันที่:</strong> ${date_get}:${time_get}<br>
      <strong>แหล่งที่มา: กรมควบคุมมลพิษ</strong>
    `;
  });

  // Reset info box when not hovering
  map.on('mouseleave', 'airquality-layer', () => {
    document.getElementById('info').innerHTML = 'Hover over the map to see AQI and PM2.5 data...';
  });
});
