// Initialize the map
const map = new maplibregl.Map({ // กำหนดค่าแผนที่
    container: 'map', // ID of the map container
    style: 'https://va-cdn-02.vallarismaps.com/core/api/styles/1.0-beta/styles/67972d36b7afe43c5898ff67?api_key=GdZIstfN1wV2FrFxnTIUvLaYXsYdhEstCAmMx9NrCDYP8RjTidm4K00NcPJAXZU1', // Map style
    center: [100.5018, 13.7563], // Initial center [lng, lat] (Bangkok) จุด center ของแผนที่
    zoom: 5 // Initial zoom level 
});

// Add navigation controls to the map
map.addControl(new maplibregl.NavigationControl(), 'top-left');

// Add a vector tile source for air quality data
map.on('load', () => { // โหลดแผนที่ เพื่อแสดงข้อมูล 
    map.addSource('airquality', {
        type: 'vector',
        url: 'https://va-cdn-02.vallarismaps.com/core/api/tiles/1.0-beta/tiles/67972b3bb7afe43c5898ff65/{z}/{x}/{y}?api_key=GdZIstfN1wV2FrFxnTIUvLaYXsYdhEstCAmMx9NrCDYP8RjTidm4K00NcPJAXZU1' // Replace with your vector tile URL
    });

    map.addLayer({
        id: 'airquality-layer',
        type: 'circle',
        source: '67972b3bb7afe43c5898ff65',
        'source-layer': '67972b3bb7afe43c5898ff65', // Replace with the actual source layer name
        paint: {
            'circle-radius': 10,
            'circle-color': '#00e400',
            'circle-opacity': 0.0 // Hide the circles by default
        }
    });

    // Update info box on hover
    map.on('mousemove', 'airquality-layer', (e) => {
        const features = e.features[0].properties;
        const aqi = features.aqiaqi || 'N/A';
        const pm25 = features.pm25 || 'N/A';
        const name = features.nameth || 'Unknown Location';
        const areath = features.areath || 'N/A';
        const date_get = features.date || 'N/A';
        const time_get = features.time || 'N/A';


        document.getElementById('info').innerHTML = `
        <strong>Air Quality Index:</strong> ${aqi}<br>
        <strong>PM 2.5:</strong> ${pm25} µg/m³<br>
        <strong>สถานที่:</strong> ${name}<br>
        <strong>พื้นที่:</strong> ${areath}<br>
        <strong>วันที่:</strong> ${date_get}:${time_get}<br>
        <strong>แหล่งที่มา: กรมควบคุมมลพิษ</strong>
      `;
    });

    // Reset info box when not hovering over a feature ถ้าไม่ได้วางเมาส์ที่ feature จะไม่แสดงข้อมูล
    map.on('mouseleave', 'airquality-layer', () => {
        document.getElementById('info').innerHTML = 'Hover over the map to see AQI and PM2.5 data...';
    });
});
