
// เลือกเอลิเมนต์ .sidebar และ .toggle-btn
const sidebar = document.querySelector('.sidebar');
const toggleBtn = document.querySelector('.toggle-btn');
const popup = document.querySelector('.popup');
const listItem = document.querySelector('.list-item');

// เพิ่มการฟังก์ชัน click เพื่อแสดง/ซ่อน sidebar
toggleBtn.addEventListener('click', function () {
    sidebar.classList.toggle('active');
});

// เพิ่มการฟังก์ชัน click เพื่อแสดง/ซ่อน popup เมื่อคลิกที่ JSON
listItem.addEventListener('click', function () {
    if (popup.style.display === 'none' || popup.style.display === '') {
        popup.style.display = 'block'; // เปิด popup เมื่อคลิกที่ JSON
    } else {
        popup.style.display = 'none'; // ปิด popup เมื่อคลิกที่ JSON อีกครั้ง
    }
});

// เพิ่มการฟังก์ชัน Import
document.getElementById('import-button').addEventListener('click', function (event) {
    event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

    // สร้าง input element สำหรับเลือกไฟล์
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.accept = '.geojson'; // ระบุให้สามารถเลือกได้เฉพาะไฟล์ .geojson

    // เมื่อผู้ใช้เลือกไฟล์แล้ว
    fileInput.addEventListener('change', function (event) {
        const file = event.target.files[0]; // เลือกไฟล์ที่ถูกเลือก

        // ถ้ามีไฟล์ที่ถูกเลือก
        if (file) {
            const reader = new FileReader();

            // เมื่อการอ่านไฟล์เสร็จสิ้น
            reader.onload = function (event) {
                const geoJSONData = event.target.result; // ข้อมูล GeoJSON จากไฟล์

                // แสดงข้อมูล GeoJSON ใน popup
                displayGeoJSON(geoJSONData);

                // วาดเรขาคณิตจากข้อมูล GeoJSON
                drawData(geoJSONData);
            };

            // อ่านไฟล์เป็นข้อมูล URL
            reader.readAsText(file);
        }
    });

    // เปิด dialog เลือกไฟล์
    fileInput.click();
});

function drawData(geoJSONData) {
    // แปลงข้อมูล GeoJSON เป็น object
    const data = JSON.parse(geoJSONData);

    console.log("GeoJSON Data:", data);

    // วาดเรขาคณิตบนแผนที่
    draw.add(data);

    // หาตำแหน่งที่เหมาะสมในข้อมูล GeoJSON
    const bounds = new maplibregl.LngLatBounds();
    data.features.forEach(feature => {
        if (feature.geometry.type === 'Point') {
            bounds.extend(feature.geometry.coordinates);
        } else if (feature.geometry.type === 'Polygon' || feature.geometry.type === 'MultiPolygon') {
            feature.geometry.coordinates.forEach(coords => {
                coords.forEach(coord => {
                    bounds.extend(coord);
                });
            });
        }
    });

    // ทำการ zoom ไปยังตำแหน่งที่เหมาะสม
    map.fitBounds(bounds, { padding: 20 });
}

// เพิ่มการสร้าง Blob และดาวน์โหลดข้อมูล GeoJSON
document.getElementById('export-button').addEventListener('click', function (event) {
    event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

    const textarea = popup.querySelector('textarea');

    // ใช้ CodeMirror เพื่อดึงข้อมูล GeoJSON โดยไม่รวม line numbers
    const editor = CodeMirror.fromTextArea(textarea); // เพิ่มโค้ดนี้เพื่อกำหนดตัวแปร editor

    let geoJSON = editor.getValue().trim();

    // ถ้ามีข้อมูลใน popup
    if (geoJSON !== "") {
        // แสดง alert ถามผู้ใช้ว่าต้องการบันทึกหรือไม่
        const confirmSave = confirm("คุณต้องการบันทึกข้อมูล GeoJSON ลงในเครื่องของคุณหรือไม่?");
        if (confirmSave) {
            // สร้าง Blob จากข้อความ GeoJSON
            const blob = new Blob([geoJSON], { type: 'application/json' });

            // สร้าง URL สำหรับ Blob
            const blobURL = window.URL.createObjectURL(blob);

            // สร้างลิงก์สำหรับดาวน์โหลดไฟล์
            const downloadLink = document.createElement('a');
            downloadLink.href = blobURL;
            downloadLink.download = 'data.geojson'; // กำหนดชื่อไฟล์ที่จะดาวน์โหลด
            downloadLink.click();

            // ล้าง URL หลังจากดาวน์โหลดเสร็จสิ้น
            window.URL.revokeObjectURL(blobURL);

            alert("ข้อมูลถูกบันทึกแล้ว!");
        } else {
            // ถ้าผู้ใช้ยกเลิกการบันทึก
            alert("การบันทึกถูกยกเลิก");
        }
    } else {
        // ถ้าไม่มีข้อมูลใน popup
        alert("ไม่มีข้อมูล GeoJSON ที่จะบันทึก");
    }
});

// // เพิ่มการฟังก์ชัน Import to Vallaris
// document.getElementById('vallaris').addEventListener('click', function (event) {
//     event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล URL ของเซิร์ฟเวอร์ Vallaris API
//     let serverURL = prompt("กรุณากรอก URL ของเซิร์ฟเวอร์ Vallaris API:");

//     // ถ้าผู้ใช้กดปุ่ม "Cancel" หรือไม่กรอกข้อมูล
//     if (!serverURL) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก URL ของเซิร์ฟเวอร์ Vallaris API");
//         return;
//     }

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล Collection ID
//     let collectionId = prompt("กรุณากรอก Collection ID ของคอลเล็กชันที่ต้องการ:");

//     if (!collectionId) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก Collection ID");
//         return;
//     }

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล API Key
//     let apiKey = prompt("กรุณากรอก API Key ของคุณ:");

//     if (!apiKey) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก API Key");
//         return;
//     }

//     // ดึงข้อมูล GeoJSON จาก popup
//     const textarea = popup.querySelector('textarea');
//     const geoJSONData = textarea.value;

//     // เรียกใช้ฟังก์ชัน sendDataToVallaris(geoJSONData) เพื่อส่งข้อมูล GeoJSON ไปยัง Vallaris
//     sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData);
// });


// function sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData) {
//     // กำหนด header สำหรับการส่งข้อมูล
//     const Myheaders = new Headers();
//     Myheaders.append("API-Key", apiKey);
//     Myheaders.append("Content-Type", "application/json");
//     // กำหนดข้อมูล JSON ที่จะส่งไปยัง Vallaris API
//     const data = {
//         process: {
//             id: "214797b7c22908afe49950f6",
//             title: "Input Geojson",
//             description: "The features data from geojson",
//             seemore: "https://docs.vallarismaps.com/docs/vallaris-module-list/Input-Data/InputGeoJSON",
//             version: 1,
//             jobControlOptions: "async-execute",
//             outputTransmission: "value",
//             inputs: {
//                 input: [
//                     {
//                         id: "input",
//                         title: "input",
//                         description: "The features",
//                         input: [
//                             {
//                                 id: "input",
//                                 required: true,
//                                 title: "input",
//                                 format: "GeoJSON",
//                                 value: JSON.parse(geoJSONData)
//                             }
//                         ]
//                     }
//                 ],
//                 parameter: []
//             },
//             "outputs": [
//                 {
//                     "id": "result",
//                     "title": "result",
//                     "description": "The result features",
//                     "required": true,
//                     "format": "vallaris",
//                     "value": collectionId,
//                     "transmissionMode": "value"
//                 }
//             ],
//             "response": "raw",
//             "mode": "async",
//             "createTemporary": false,
//             "group": 1,
//             "createdAt": "2023-10-24T04:59:24.320Z",
//             "createdBy": "65374f2b285de6045d6ff690",
//             "updatedAt": "2023-10-24T04:59:24.320Z",
//             "updatedBy": "65374f2b285de6045d6ff690"
//         }
//     }

//     // กำหนดตัวเลือกสำหรับการส่งข้อมูลผ่าน Fetch API
//     const requestOptions = {
//         method: "POST",
//         headers: Myheaders,
//         body: JSON.stringify(data),
//         redirect: "follow"
//     };

//     console.log(requestOptions)

//     // ส่งข้อมูลไปยัง Vallaris API โดยใช้ Fetch API
//     fetch(serverURL + "/core/api/processes/1.0-beta/processes/214797b7c22908afe49950f6/jobs", requestOptions)
//         .then(response => {
//             if (response.ok) {
//                 // ถ้าการส่งข้อมูลสำเร็จ
//                 alert("การนำเข้าข้อมูลสำเร็จ!");
//             } else {
//                 // ถ้าการส่งข้อมูลไม่สำเร็จ
//                 alert("เกิดข้อผิดพลาดในการนำเข้าข้อมูล");
//             }
//         })
//         .catch(error => {
//             console.error("เกิดข้อผิดพลาดในการส่งข้อมูล:", error);
//             alert("เกิดข้อผิดพลาดในการส่งข้อมูล");
//         });
// }

// เพิ่มการฟังก์ชัน Import to Vallaris
document.getElementById('vallaris').addEventListener('click', function (event) {
    event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

    // ดึงข้อมูล URL และค่าอื่น ๆ จาก input ที่ผู้ใช้กรอก
    let serverURL = prompt("กรุณากรอก URL ของเซิร์ฟเวอร์ Vallaris API:");
    // ตรวจสอบค่า serverURL และดำเนินการต่อเมื่อมีค่าที่ถูกต้อง
    if (!serverURL) {
        alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก URL ของเซิร์ฟเวอร์ Vallaris API");
        return;
    }

    // ดึงข้อมูล Collection ID และ API Key จากผู้ใช้
    let collectionId = prompt("กรุณากรอก Collection ID ของคอลเล็กชันที่ต้องการ:");
    let apiKey = prompt("กรุณากรอก API Key ของคุณ:");

    // ตรวจสอบค่า collectionId และ apiKey และดำเนินการต่อเมื่อมีค่าที่ถูกต้อง
    if (!collectionId || !apiKey) {
        alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก Collection ID หรือ API Key");
        return;
    }

    // ดึงข้อมูล GeoJSON จาก popup
    const textarea = popup.querySelector('textarea');
    const geoJSONData = textarea.value;

    // เรียกใช้ showPercentage() เพื่อแสดง% ของข้อมูลที่นำเข้าก่อนส่งข้อมูลไปยัง Vallaris
    showPercentage(0); // เริ่มที่ 0%

    // เรียกใช้ฟังก์ชัน sendDataToVallaris เพื่อส่งข้อมูลไปยัง Vallaris
    sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData);
});

function sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData) {
    // กำหนด header สำหรับการส่งข้อมูล
    const Myheaders = new Headers();
    Myheaders.append("API-Key", apiKey);
    Myheaders.append("Content-Type", "application/json");
    // กำหนดข้อมูล JSON ที่จะส่งไปยัง Vallaris API
    const data = {
        process: {
            id: "214797b7c22908afe49950f6",
            title: "Input Geojson",
            description: "The features data from geojson",
            seemore: "https://docs.vallarismaps.com/docs/vallaris-module-list/Input-Data/InputGeoJSON",
            version: 1,
            jobControlOptions: "async-execute",
            outputTransmission: "value",
            inputs: {
                input: [
                    {
                        id: "input",
                        title: "input",
                        description: "The features",
                        input: [
                            {
                                id: "input",
                                required: true,
                                title: "input",
                                format: "GeoJSON",
                                value: JSON.parse(geoJSONData)
                            }
                        ]
                    }
                ],
                parameter: []
            },
            "outputs": [
                {
                    "id": "result",
                    "title": "result",
                    "description": "The result features",
                    "required": true,
                    "format": "vallaris",
                    "value": collectionId,
                    "transmissionMode": "value"
                }
            ],
            "response": "raw",
            "mode": "async",
            "createTemporary": false,
            "group": 1,
            "createdAt": "2023-10-24T04:59:24.320Z",
            "createdBy": "65374f2b285de6045d6ff690",
            "updatedAt": "2023-10-24T04:59:24.320Z",
            "updatedBy": "65374f2b285de6045d6ff690"
        }
    }

    // กำหนดตัวเลือกสำหรับการส่งข้อมูลผ่าน Fetch API
    const requestOptions = {
        method: "POST",
        headers: Myheaders,
        body: JSON.stringify(data),
        redirect: "follow"
    };

    console.log(requestOptions)

    // ส่งข้อมูลไปยัง Vallaris API โดยใช้ Fetch API
    fetch(serverURL + "/core/api/processes/1.0-beta/processes/214797b7c22908afe49950f6/jobs", requestOptions)
        .then(response => {
            // ตรวจสอบสถานะของการตอบกลับ
            if (response.ok) {
                // ถ้าการส่งข้อมูลสำเร็จ
                alert("การนำเข้าข้อมูลสำเร็จ!");
            } else {
                // ถ้าการส่งข้อมูลไม่สำเร็จ
                alert("เกิดข้อผิดพลาดในการนำเข้าข้อมูล");
            }
            // เรียกใช้ hidePercentage() เพื่อซ่อน% ของข้อมูลที่นำเข้าหลังจากแสดง alert
            hidePercentage();
        })
        .catch(error => {
            console.error("เกิดข้อผิดพลาดในการส่งข้อมูล:", error);
            alert("เกิดข้อผิดพลาดในการส่งข้อมูล");
            // เรียกใช้ hidePercentage() เพื่อซ่อน% ของข้อมูลที่นำเข้าหลังจากแสดง alert
            hidePercentage();
        });
}

function showPercentage(percentage) {
    // แสดง% ของข้อมูลที่นำเข้า
    console.log(`Percentage of data imported: ${percentage}%`);
}

function hidePercentage() {
    // ซ่อน% ของข้อมูลที่นำเข้า
    console.log("Hide percentage");
}
// document.getElementById('vallaris').addEventListener('click', function (event) {
//     event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

//     // ดึงข้อมูล URL และค่าอื่น ๆ จาก input ที่ผู้ใช้กรอก
//     let serverURL = prompt("กรุณากรอก URL ของเซิร์ฟเวอร์ Vallaris API:");
//     // ตรวจสอบค่า serverURL และดำเนินการต่อเมื่อมีค่าที่ถูกต้อง
//     if (!serverURL) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก URL ของเซิร์ฟเวอร์ Vallaris API");
//         return;
//     }

//     // ดึงข้อมูล Collection ID และ API Key จากผู้ใช้
//     let collectionId = prompt("กรุณากรอก Collection ID ของคอลเล็กชันที่ต้องการ:");
//     let apiKey = prompt("กรุณากรอก API Key ของคุณ:");

//     // ตรวจสอบค่า collectionId และ apiKey และดำเนินการต่อเมื่อมีค่าที่ถูกต้อง
//     if (!collectionId || !apiKey) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก Collection ID หรือ API Key");
//         return;
//     }

//     // ดึงข้อมูล GeoJSON จาก popup
//     const textarea = popup.querySelector('textarea');
//     const geoJSONData = textarea.value;

//     // แสดง spinner ก่อนส่งข้อมูล
//     showSpinner();

//     // เรียกใช้ฟังก์ชัน sendDataToVallaris เพื่อส่งข้อมูลไปยัง Vallaris
//     sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData);
// });

// function sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData) {
//     // กำหนด header สำหรับการส่งข้อมูล
//     const Myheaders = new Headers();
//     Myheaders.append("API-Key", apiKey);
//     Myheaders.append("Content-Type", "application/json");
//     // กำหนดข้อมูล JSON ที่จะส่งไปยัง Vallaris API
//     const data = {
//         process: {
//             id: "214797b7c22908afe49950f6",
//             title: "Input Geojson",
//             description: "The features data from geojson",
//             seemore: "https://docs.vallarismaps.com/docs/vallaris-module-list/Input-Data/InputGeoJSON",
//             version: 1,
//             jobControlOptions: "async-execute",
//             outputTransmission: "value",
//             inputs: {
//                 input: [
//                     {
//                         id: "input",
//                         title: "input",
//                         description: "The features",
//                         input: [
//                             {
//                                 id: "input",
//                                 required: true,
//                                 title: "input",
//                                 format: "GeoJSON",
//                                 value: JSON.parse(geoJSONData)
//                             }
//                         ]
//                     }
//                 ],
//                 parameter: []
//             },
//             "outputs": [
//                 {
//                     "id": "result",
//                     "title": "result",
//                     "description": "The result features",
//                     "required": true,
//                     "format": "vallaris",
//                     "value": collectionId,
//                     "transmissionMode": "value"
//                 }
//             ],
//             "response": "raw",
//             "mode": "async",
//             "createTemporary": false,
//             "group": 1,
//             "createdAt": "2023-10-24T04:59:24.320Z",
//             "createdBy": "65374f2b285de6045d6ff690",
//             "updatedAt": "2023-10-24T04:59:24.320Z",
//             "updatedBy": "65374f2b285de6045d6ff690"
//         }
//     }

//     // กำหนดตัวเลือกสำหรับการส่งข้อมูลผ่าน Fetch API
//     const requestOptions = {
//         method: "POST",
//         headers: Myheaders,
//         body: JSON.stringify(data),
//         redirect: "follow"
//     };

//     // ส่งข้อมูลไปยัง Vallaris API โดยใช้ Fetch API
//     fetch(serverURL + "/core/api/processes/1.0-beta/processes/214797b7c22908afe49950f6/jobs", requestOptions)
//         .then(response => {
//             if (response.ok) {
//                 // ถ้าการส่งข้อมูลสำเร็จ
//                 alert("การนำเข้าข้อมูลสำเร็จ!");
//             } else {
//                 // ถ้าการส่งข้อมูลไม่สำเร็จ
//                 alert("เกิดข้อผิดพลาดในการนำเข้าข้อมูล");
//             }
//             // ซ่อน spinner เมื่อสำเร็จหรือไม่สำเร็จ
//             hideSpinner();
//         })
//         .catch(error => {
//             console.error("เกิดข้อผิดพลาดในการส่งข้อมูล:", error);
//             alert("เกิดข้อผิดพลาดในการส่งข้อมูล");
//             // ซ่อน spinner เมื่อไม่สามารถส่งข้อมูลได้
//             hideSpinner();
//         });
// }

// // เพิ่ม/ลบ class hidden เพื่อแสดง/ซ่อน spinner
// function showSpinner() {
//     document.getElementById('loading-spinner').classList.remove('hidden');
// }

// function hideSpinner() {
//     document.getElementById('loading-spinner').classList.add('hidden');
// }

function resetMap() {
    // Clear the GeoJSON display
    popup.innerText = '';
    popup.style.display = 'none';

    // Remove all drawn geometries
    draw.deleteAll();
}

// // เลือกเอลิเมนต์ .sidebar และ .toggle-btn
// const sidebar = document.querySelector('.sidebar');
// const toggleBtn = document.querySelector('.toggle-btn');
// const popup = document.querySelector('.popup');
// const listItem = document.querySelector('.list-item');

// // เพิ่มการฟังก์ชัน click เพื่อแสดง/ซ่อน sidebar
// toggleBtn.addEventListener('click', function () {
//     sidebar.classList.toggle('active');
// });

// // เพิ่มการฟังก์ชัน click เพื่อแสดง/ซ่อน popup เมื่อคลิกที่ JSON
// listItem.addEventListener('click', function () {
//     if (popup.style.display === 'none' || popup.style.display === '') {
//         popup.style.display = 'block'; // เปิด popup เมื่อคลิกที่ JSON
//     } else {
//         popup.style.display = 'none'; // ปิด popup เมื่อคลิกที่ JSON อีกครั้ง
//     }
// });

// // เพิ่มการฟังก์ชัน Import
// document.getElementById('import-button').addEventListener('click', function (event) {
//     event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

//     // สร้าง input element สำหรับเลือกไฟล์
//     const fileInput = document.createElement('input');
//     fileInput.type = 'file';
//     fileInput.accept = '.geojson'; // ระบุให้สามารถเลือกได้เฉพาะไฟล์ .geojson

//     // เมื่อผู้ใช้เลือกไฟล์แล้ว
//     fileInput.addEventListener('change', function (event) {
//         const file = event.target.files[0]; // เลือกไฟล์ที่ถูกเลือก

//         // ถ้ามีไฟล์ที่ถูกเลือก
//         if (file) {
//             const reader = new FileReader();

//             // เมื่อการอ่านไฟล์เสร็จสิ้น
//             reader.onload = function (event) {
//                 const geoJSONData = event.target.result; // ข้อมูล GeoJSON จากไฟล์

//                 // แสดงข้อมูล GeoJSON ใน popup
//                 displayGeoJSON(geoJSONData);

//                 // วาดเรขาคณิตจากข้อมูล GeoJSON
//                 drawData(geoJSONData);
//             };

//             // อ่านไฟล์เป็นข้อมูล URL
//             reader.readAsText(file);
//         }
//     });

//     // เปิด dialog เลือกไฟล์
//     fileInput.click();
// });

// function drawData(geoJSONData) {
//     // แปลงข้อมูล GeoJSON เป็น object
//     const data = JSON.parse(geoJSONData);

//     console.log("GeoJSON Data:", data);

//     // วาดเรขาคณิตบนแผนที่
//     draw.add(data);

//     // หาตำแหน่งที่เหมาะสมในข้อมูล GeoJSON
//     const bounds = new maplibregl.LngLatBounds();
//     data.features.forEach(feature => {
//         if (feature.geometry.type === 'Point') {
//             bounds.extend(feature.geometry.coordinates);
//         } else if (feature.geometry.type === 'Polygon' || feature.geometry.type === 'MultiPolygon') {
//             feature.geometry.coordinates.forEach(coords => {
//                 coords.forEach(coord => {
//                     bounds.extend(coord);
//                 });
//             });
//         }
//     });

//     // ทำการ zoom ไปยังตำแหน่งที่เหมาะสม
//     map.fitBounds(bounds, { padding: 20 });
// }

// // เพิ่มการสร้าง Blob และดาวน์โหลดข้อมูล GeoJSON
// document.getElementById('export-button').addEventListener('click', function (event) {
//     event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

//     const textarea = popup.querySelector('textarea');

//     // ใช้ CodeMirror เพื่อดึงข้อมูล GeoJSON โดยไม่รวม line numbers
//     const editor = CodeMirror.fromTextArea(textarea); // เพิ่มโค้ดนี้เพื่อกำหนดตัวแปร editor

//     let geoJSON = editor.getValue().trim();

//     // ถ้ามีข้อมูลใน popup
//     if (geoJSON !== "") {
//         // แสดง alert ถามผู้ใช้ว่าต้องการบันทึกหรือไม่
//         const confirmSave = confirm("คุณต้องการบันทึกข้อมูล GeoJSON ลงในเครื่องของคุณหรือไม่?");
//         if (confirmSave) {
//             // สร้าง Blob จากข้อความ GeoJSON
//             const blob = new Blob([geoJSON], { type: 'application/json' });

//             // สร้าง URL สำหรับ Blob
//             const blobURL = window.URL.createObjectURL(blob);

//             // สร้างลิงก์สำหรับดาวน์โหลดไฟล์
//             const downloadLink = document.createElement('a');
//             downloadLink.href = blobURL;
//             downloadLink.download = 'data.geojson'; // กำหนดชื่อไฟล์ที่จะดาวน์โหลด
//             downloadLink.click();

//             // ล้าง URL หลังจากดาวน์โหลดเสร็จสิ้น
//             window.URL.revokeObjectURL(blobURL);

//             alert("ข้อมูลถูกบันทึกแล้ว!");
//         } else {
//             // ถ้าผู้ใช้ยกเลิกการบันทึก
//             alert("การบันทึกถูกยกเลิก");
//         }
//     } else {
//         // ถ้าไม่มีข้อมูลใน popup
//         alert("ไม่มีข้อมูล GeoJSON ที่จะบันทึก");
//     }
// });

// // เพิ่มการฟังก์ชัน Import to Vallaris
// document.getElementById('vallaris').addEventListener('click', function (event) {
//     event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล URL ของเซิร์ฟเวอร์ Vallaris API
//     let serverURL = prompt("กรุณากรอก URL ของเซิร์ฟเวอร์ Vallaris API:");

//     // ถ้าผู้ใช้กดปุ่ม "Cancel" หรือไม่กรอกข้อมูล
//     if (!serverURL) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก URL ของเซิร์ฟเวอร์ Vallaris API");
//         return; // ออกจากฟังก์ชัน
//     }

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล Collection ID
//     let collectionId = prompt("กรุณากรอก Collection ID ของคอลเล็กชันที่ต้องการ:");

//     // ถ้าผู้ใช้กดปุ่ม "Cancel" หรือไม่กรอกข้อมูล
//     if (!collectionId) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก Collection ID");
//         return; // ออกจากฟังก์ชัน
//     }

//     // แสดง prompt dialog เพื่อให้ผู้ใช้กรอกข้อมูล API Key
//     let apiKey = prompt("กรุณากรอก API Key ของคุณ:");

//     // ถ้าผู้ใช้กดปุ่ม "Cancel" หรือไม่กรอกข้อมูล
//     if (!apiKey) {
//         alert("การนำเข้าถูกยกเลิก เนื่องจากไม่ได้กรอก API Key");
//         return; // ออกจากฟังก์ชัน
//     }

//     // ดึงข้อมูล GeoJSON จาก popup
//     const textarea = popup.querySelector('textarea');
//     const geoJSONData = textarea.value;

//     // เรียกใช้ฟังก์ชัน sendDataToVallaris(geoJSONData) เพื่อส่งข้อมูล GeoJSON ไปยัง Vallaris
//     sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData);
// });


// function sendDataToVallaris(apiKey, collectionId, serverURL, geoJSONData) {
//     // กำหนด header สำหรับการส่งข้อมูล
//     const Myheaders = new Headers();
//     Myheaders.append("API-Key", apiKey);
//     Myheaders.append("Content-Type", "application/json");
//     // กำหนดข้อมูล JSON ที่จะส่งไปยัง Vallaris API
//     const data = {
//         process: {
//             id: "214797b7c22908afe49950f6",
//             title: "Input Geojson",
//             description: "The features data from geojson",
//             seemore: "https://docs.vallarismaps.com/docs/vallaris-module-list/Input-Data/InputGeoJSON",
//             version: 1,
//             jobControlOptions: "async-execute",
//             outputTransmission: "value",
//             inputs: {
//                 input: [
//                     {
//                         id: "input",
//                         title: "input",
//                         description: "The features",
//                         input: [
//                             {
//                                 id: "input",
//                                 required: true,
//                                 title: "input",
//                                 format: "GeoJSON",
//                                 value: JSON.parse(geoJSONData)
//                             }
//                         ]
//                     }
//                 ],
//                 parameter: []
//             },
//             "outputs": [
//                 {
//                     "id": "result",
//                     "title": "result",
//                     "description": "The result features",
//                     "required": true,
//                     "format": "vallaris",
//                     "value": collectionId,
//                     "transmissionMode": "value"
//                 }
//             ],
//             "response": "raw",
//             "mode": "async",
//             "createTemporary": false,
//             "group": 1,
//             "createdAt": "2023-10-24T04:59:24.320Z",
//             "createdBy": "65374f2b285de6045d6ff690",
//             "updatedAt": "2023-10-24T04:59:24.320Z",
//             "updatedBy": "65374f2b285de6045d6ff690"
//         }
//     }

//     // กำหนดตัวเลือกสำหรับการส่งข้อมูลผ่าน Fetch API
//     const requestOptions = {
//         method: "POST",
//         headers: Myheaders,
//         body: JSON.stringify(data),
//         redirect: "follow"
//     };

//     console.log(requestOptions)

//     // ส่งข้อมูลไปยัง Vallaris API โดยใช้ Fetch API
//     fetch(serverURL + "/core/api/processes/1.0-beta/processes/214797b7c22908afe49950f6/jobs", requestOptions)
//         .then(response => {
//             if (response.ok) {
//                 alert("การนำเข้าข้อมูลสำเร็จ!");
//                 return response.json();
//             } else {
//                 throw new Error('เกิดข้อผิดพลาดในการส่งข้อมูล: ' + response.statusText);
//             }
//         })
//         .then(data => {
//             checkImportStatus(data.jobId); // เรียกใช้ฟังก์ชันเพื่อตรวจสอบสถานะของงาน
//             displayStatus('กำลังตรวจสอบสถานะการนำเข้าข้อมูล...');
//         })
//         .catch(error => {
//             console.error('เกิดข้อผิดพลาดในการส่งข้อมูล:', error);
//             alert('เกิดข้อผิดพลาดในการส่งข้อมูล');
//         });
// }

// function checkImportStatus(jobId) {
//     const statusURL = serverURL + '/core/api/processes/1.0-beta/jobs/' + jobId;

//     const requestOptions = {
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json',
//             'API-Key': apiKey
//         }
//     };

//     fetch(statusURL, requestOptions)
//         .then(response => {
//             if (response.ok) {
//                 return response.json();
//             } else {
//                 throw new Error('เกิดข้อผิดพลาดในการเรียกใช้ API: ' + response.statusText);
//             }
//         })
//         .then(data => {
//             if (data.status === 'completed') {
//                 alert('การนำเข้าข้อมูลสำเร็จแล้ว');
//                 displayStatus('การนำเข้าข้อมูลสำเร็จแล้ว');
//             } else if (data.status === 'failed') {
//                 alert('การนำเข้าข้อมูลล้มเหลว');
//                 displayStatus('การนำเข้าข้อมูลล้มเหลว');
//             } else {
//                 alert('การนำเข้าข้อมูลกำลังดำเนินการอยู่');
//                 displayStatus('การนำเข้าข้อมูลกำลังดำเนินการอยู่');
//             }
//         })
//         .catch(error => {
//             console.error('เกิดข้อผิดพลาดในการตรวจสอบสถานะ:', error);
//             alert('เกิดข้อผิดพลาดในการตรวจสอบสถานะ');
//         });
// }

// function displayStatus(statusMessage) {
//     // สร้าง element สำหรับแสดงข้อความ status
//     const statusElement = document.createElement('div');
//     statusElement.textContent = 'สถานะ: ' + statusMessage;

//     // เพิ่ม element ลงใน DOM
//     document.body.appendChild(statusElement);
// }


// function resetMap() {
//     // Clear the GeoJSON display
//     popup.innerText = '';
//     popup.style.display = 'none';

//     // Remove all drawn geometries
//     draw.deleteAll();
// }