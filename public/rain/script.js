// IDs สำหรับข้อมูลแต่ละวัน
const dailyData = [
    "6707760141687b18e01c0150",
    "67076b848430e97964742342",
    "67076ba57cfc9ce4e761f001",
    "67076bc1ba2bbbf1bbe73d73",
    "67076bec8430e97964742343",
    "67076c157cfc9ce4e761f002",
    "67076c31ba2bbbf1bbe73d74"
];

// ใส่ API Key (ควรเก็บ API Key อย่างปลอดภัย)
const api_key = "f6D7gNW3AJoLtaUUgn6b5iIS39OBpMlnzPQSUddblSOVbcH4Popk8Q4NMcf5CTXW";

// สร้างแผนที่
const map = new maplibregl.Map({
    container: 'map',
    style: `https://va-cdn-02.vallarismaps.com/core/api/styles/1.0-beta/styles/66d923714a092f62e2ce45a4?api_key=sd177Wi0tvPwRFW923IX3fNzxBsPKHM8qkloKUEnHt4UvSnYPogFcVZtXFwvcygx`,
    center: [100.523186, 13.736717], // กรุงเทพ
    zoom: 5,
});
map.addControl(new maplibregl.NavigationControl());
map.addControl(
    new maplibregl.GeolocateControl({
        positionOptions: { enableHighAccuracy: true },
        trackUserLocation: true
    })
);

// ฟังก์ชันสร้างวันที่
function generateNext7Days() {
    const today = new Date();
    return Array.from({ length: 7 }, (_, i) => {
        const nextDate = new Date(today);
        nextDate.setDate(today.getDate() + i + 1);
        return nextDate.toLocaleDateString('en-US', { year: 'numeric', month: 'short', day: 'numeric' });
    });
}

// สร้างแถบปุ่มวันที่
const dateBar = document.getElementById('dateBar');
const infoBox = document.getElementById('info');
const dates = generateNext7Days();

dates.forEach((date, i) => {
    const button = document.createElement('button');
    button.textContent = date;
    button.classList.add('date-button');
    button.dataset.day = i;

    button.onclick = async () => {
        switchDailyLayer(i);
        infoBox.textContent = "Loading data, please wait...";
        await fetchUpdatedAt(dailyData[i]);
    };

    dateBar.appendChild(button);
});

// ฟังก์ชันเปลี่ยนเลเยอร์ในแผนที่
function switchDailyLayer(dayIndex) {
    document.querySelectorAll('.date-button').forEach(btn => btn.classList.remove('active'));
    document.querySelector(`button[data-day="${dayIndex}"]`).classList.add('active');

    const currentLayerId = "daily-layer";

    if (map.getLayer(currentLayerId)) {
        map.removeLayer(currentLayerId);
        map.removeSource(currentLayerId);
    }

    const tmsUrl = `https://vallaris.dragonfly.gistda.or.th/core/api/maps/coverage/1.0-beta/maps/${dailyData[dayIndex]}/tms/{z}/{x}/{y}?api_key=${api_key}`;
    map.addSource(currentLayerId, { type: 'raster', tiles: [tmsUrl], tileSize: 256 });
    map.addLayer({ id: currentLayerId, type: 'raster', source: currentLayerId, paint: { 'raster-opacity': 0.4 } });

    console.log(`Switched to TMS for day index ${dayIndex}: ${tmsUrl}`);
}

// ฟังก์ชันดึงข้อมูล "updatedAt" จาก API
async function fetchUpdatedAt() {
    // URL สำหรับดึงข้อมูลจาก API
    const url = `https://vallaris.dragonfly.gistda.or.th/core/api/processes/1.0-beta/schedule/7705b79f612d46ee9e621ed9/jobs?api_key=${api_key}`;

    try {
        // ดึงข้อมูลจาก URL
        const response = await fetch(url);
        
        // ตรวจสอบว่า HTTP response status เป็น 200 หรือไม่
        if (!response.ok) {
            throw new Error(`Error fetching data: ${response.status}`);
        }

        // แปลงข้อมูล JSON
        const data = await response.json();

        // ตรวจสอบว่า data มีรายการหรือไม่
        if (data && data.length > 0) {
            // ดึงข้อมูลตัวสุดท้ายจาก array
            const latestJob = data[data.length - 1];
            
            // ดึงค่า updatedAt จากข้อมูลตัวสุดท้าย
            const updatedAt = latestJob.updatedAt || "No update date available";
            
            // อัปเดตข้อความใน infoBox
            infoBox.textContent = `Processed on: ${new Date(updatedAt).toLocaleString()}`;
        } else {
            infoBox.textContent = "No job data available.";
        }
    } catch (error) {
        console.error(error);
        infoBox.textContent = "Error fetching updated date.";
    }
}


let playInterval = null; // ใช้เก็บตัวจับเวลาสำหรับปุ่ม Play
let currentDayIndex = 0; // เก็บค่าของวันที่ที่แสดงปัจจุบัน

// ฟังก์ชันเริ่มต้นการเล่น
function startPlay() {
    const playButton = document.getElementById('playButton');
    playButton.textContent = "Pause";
    playInterval = setInterval(() => {
        switchDailyLayer(currentDayIndex); // แสดงข้อมูลของวันที่ปัจจุบัน
        fetchUpdatedAt(dailyData[currentDayIndex]); // ดึงข้อมูล
        currentDayIndex = (currentDayIndex + 1) % dailyData.length; // เลื่อนลำดับวันที่ (วนกลับไปเริ่มต้น)
    }, 2000); // ตั้งเวลาเป็น 2 วินาทีต่อวัน
}

// ฟังก์ชันหยุดการเล่น
function stopPlay() {
    const playButton = document.getElementById('playButton');
    playButton.textContent = "Play";
    clearInterval(playInterval);
    playInterval = null;
}

// จัดการการคลิกปุ่ม Play/Pause
document.getElementById('playButton').addEventListener('click', () => {
    if (playInterval) {
        stopPlay(); // ถ้ามีการเล่นอยู่ให้หยุด
    } else {
        startPlay(); // ถ้ายังไม่ได้เล่นให้เริ่มเล่น
    }
});

// เพิ่ม GeoJSON สำหรับตำแหน่ง Pin
const pinSourceId = "clickPin";

// ฟังก์ชันสำหรับเพิ่ม Pin ครั้งแรก
function initializePinLayer() {
    if (!map.getSource(pinSourceId)) {
        // เพิ่ม source สำหรับตำแหน่ง Pin
        map.addSource(pinSourceId, {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: [], // ไม่มีข้อมูลเริ่มต้น
            },
        });

        // เพิ่มเลเยอร์เพื่อแสดง Pin
        map.addLayer({
            id: pinSourceId,
            type: 'circle',
            source: pinSourceId,
            paint: {
                'circle-radius': 8,
                'circle-color': '#ff5722',
                'circle-stroke-width': 2,
                'circle-stroke-color': '#ffffff',
            },
        });
    }
}

// เรียกใช้เมื่อต้องการอัปเดตตำแหน่ง Pin
function updatePinLocation(lng, lat) {
    const source = map.getSource(pinSourceId);

    // อัปเดตข้อมูล GeoJSON
    source.setData({
        type: 'FeatureCollection',
        features: [
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [lng, lat], // ตำแหน่งใหม่
                },
            },
        ],
    });
}

// เรียกใช้ฟังก์ชันสำหรับสร้าง Pin Layer เมื่อโหลดแผนที่
map.on('load', () => {
    initializePinLayer();
});

// ฟังก์ชันที่ทำงานเมื่อคลิกบนแผนที่
map.on('click', function (e) {
    const lngLat = e.lngLat; // รับพิกัดที่ผู้ใช้คลิก
    const x = lngLat.lng.toFixed(4); // พิกัด X (Longitude)
    const y = lngLat.lat.toFixed(4); // พิกัด Y (Latitude)

    // อัปเดตแถบข้อมูล (info box) เพื่อแสดงค่า X, Y
    infoBox.textContent = `At: Longitude: ${x}, Latitude: ${y}`;

    // อัปเดตตำแหน่ง Pin บนแผนที่
    updatePinLocation(lngLat.lng, lngLat.lat);
});

