// ใช้ fetch เพื่อโหลดข้อมูล JSON
fetch('data.json')
.then(response => response.json())
.then(rawData => {
    // จัดกลุ่ม process และนับสถานะ
    const processes = [...new Set(rawData.map(item => item.process))];
    const statuses = ["successful", "failed", "running"];

    // ฟังก์ชันหาข้อมูล updatedAt
    const getUpdatedAt = (process, status) => {
        const item = rawData.find(data => data.process === process && data.status === status);
        return item ? item.updatedAt : "N/A";
    };

    // จัดหมวดหมู่ข้อมูลตาม Process และสถานะ
    const datasets = statuses.map(status => ({
        label: status.charAt(0).toUpperCase() + status.slice(1),
        data: processes.map(process => rawData.filter(item => item.process === process && item.status === status).length),
        backgroundColor: status === "successful" ? "#4caf50" : status === "failed" ? "#f44336" : "#ff9800",
        stack: "Stack 0"
    }));

    // สรุปจำนวนสถานะทั้งหมด
    const totalStatus = rawData.reduce((acc, item) => {
        acc[item.status]++;
        return acc;
    }, { successful: 0, failed: 0, running: 0 });

    // เตรียมข้อมูล Doughnut Chart
    const statusSummaryData = {
        labels: ["Successful", "Failed", "Running"],
        datasets: [{
            data: [totalStatus.successful, totalStatus.failed, totalStatus.running],
            backgroundColor: ['#4caf50', '#f44336', '#ff9800'],
            hoverOffset: 10,
            borderColor: '#fff',
            borderWidth: 2
        }]
    };

    // สร้างกราฟ Pipeline Status by Process
    const ctx1 = document.getElementById('pipelineProcessChart').getContext('2d');
    new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: processes,
            datasets: datasets
        },
        options: {
            responsive: true,
            plugins: {
                legend: { position: 'top' },
                tooltip: {
                    callbacks: {
                        // ฟังก์ชันเพื่อเพิ่มข้อมูล `updatedAt` ใน Tooltip
                        label: (context) => {
                            const label = context.dataset.label || '';
                            const value = context.raw;
                            const updatedAt = getUpdatedAt(context.label, label.toLowerCase());
                            return `${label}: ${value}\nUpdated At: ${updatedAt}`;
                        }
                    }
                }
            },
            scales: {
                x: { stacked: true, title: { display: true, text: 'Process' } },
                y: { stacked: true, title: { display: true, text: 'Count' } }
            }
        }
    });

    // สร้างกราฟ Total Status Summary
    const ctx2 = document.getElementById('statusSummaryChart').getContext('2d');
    new Chart(ctx2, {
        type: 'doughnut',
        data: statusSummaryData,
        options: {
            responsive: true,
            plugins: {
                legend: { position: 'top' },
                tooltip: {
                    callbacks: {
                        label: (context) => {
                            const label = context.label || '';
                            const value = context.raw;
                            return `${label}: ${value}`;
                        }
                    }
                }
            }
        }
    });

    // 1. คำนวณค่า Duration (seconds) สำหรับแต่ละ process
    const durationData = rawData.map(item => ({
        process: item.process,
        duration: item["Duration(seconds)"]
    }));

    // 2. จัดเรียงจากมากไปน้อย
    const sortedDurationData = durationData
        .reduce((acc, { process, duration }) => {
            if (!acc[process]) acc[process] = 0;
            acc[process] += duration;
            return acc;
        }, {});

    // 3. แปลงข้อมูลให้อยู่ในรูปแบบที่ใช้กับ Chart.js และเลือก 10 อันดับแรก
    const sortedProcesses = Object.keys(sortedDurationData)
        .map(process => ({ process, duration: sortedDurationData[process] }))
        .sort((a, b) => b.duration - a.duration) // เรียงจากมากไปน้อย
        .slice(0, 10); // เลือกแค่ 10 อันดับแรก

    // เตรียมข้อมูลสำหรับกราฟ Duration Chart
    const durationChartData = {
        labels: sortedProcesses.map(item => item.process),
        datasets: [{
            label: 'Duration (seconds)',
            data: sortedProcesses.map(item => item.duration),
            backgroundColor: '#2196f3',
            borderColor: '#fff',
            borderWidth: 2
        }]
    };

    // สร้างกราฟ Top 10 Duration (seconds)
    const ctx3 = document.getElementById('durationChart').getContext('2d');
    new Chart(ctx3, {
        type: 'bar',
        data: durationChartData,
        options: {
            responsive: true,
            plugins: {
                legend: { position: 'top' },
                tooltip: {
                    callbacks: {
                        label: (context) => {
                            const process = context.label || '';
                            const duration = context.raw;
                            return `${process}: ${duration} seconds`;
                        }
                    }
                }
            },
            scales: {
                x: { title: { display: true, text: 'Process' } },
                y: { title: { display: true, text: 'Duration (seconds)' } }
            }
        }
    });

    const tableBody = document.getElementById('processTableBody');
    rawData.forEach(item => {
        const row = document.createElement('tr');
        
        // Process
        const processCell = document.createElement('td');
        processCell.textContent = item.process || 'null';  // แสดง "null" ถ้าไม่มีค่า
        row.appendChild(processCell);

        // Created At
        const createdAtCell = document.createElement('td');
        createdAtCell.textContent = item.createdAt ? new Date(item.createdAt).toLocaleString() : 'null';  // แสดง "null" ถ้าไม่มีค่า
        row.appendChild(createdAtCell);

        // Status
        const statusCell = document.createElement('td');
        statusCell.textContent = item.status || 'null';  // แสดง "null" ถ้าไม่มีค่า
        row.appendChild(statusCell);

        tableBody.appendChild(row);
    });
})
.catch(error => {
    console.error('Error loading the JSON data:', error);
});